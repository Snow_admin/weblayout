<!DOCTYPE html>
<html lang="en">
   
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>AAC-Mockup</title>
      <link rel="icon" href="http://66.42.60.129/template/favicon.ico" type="image/x-icon">
      <link rel="stylesheet" href="assets/css/style.css">
      
      <style>
          header .navbar-brand img {
            height: 65px;
            margin-left: 30px;
          }
      </style>
      
   </head>
   <body id="dark">
      <header class="dark-bb">
         <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="exchange-dark.html">
               <img src="assets/img/logo.png" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerMenu"
               aria-controls="headerMenu" aria-expanded="false" aria-label="Toggle navigation">
            <i class="icon ion-md-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="headerMenu">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:;"> MARKETS </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:;"> DASHBOARD </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:;"> FINANCES </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:;"> REGISTER </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:;"> LOGIN </a>
                  </li>
               </ul>
            </div>
         </nav>
      </header>