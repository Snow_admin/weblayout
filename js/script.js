$(document).ready(function() {
    $('.dropdown-ll').each(function (key, dropdown) {
        var $dropdown = $(dropdown);
        $dropdown.find('.dropdown-menu a').on('click', function () {
            $dropdown.find('.dd-link').text($(this).text());
        });
    });

    $('#enable-disable-2fa').change(function(){
        console.log(1)
        var code2fa = $('.wrapper-2fa-code');
        if (this.checked) {
            code2fa.hide();
        } else {
            code2fa.show();
        }
    });
});